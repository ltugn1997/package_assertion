part of assertion;

bool checkNullNumber(num item){
  try{
    if(item == null){
      return true;
    }
    if(item * 0 == 0){
      return false;
    }
    return true;
  }
  catch (error){
    throw(error);
  }
}